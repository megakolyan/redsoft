import Vue from 'vue'
import App from './App.vue'
import axios from 'axios';
import '@/assets/scss/styles.scss'
Vue.config.productionTip = false
//Vue.use(axios);
new Vue({
  axios,
  render: h => h(App),
}).$mount('#app')
